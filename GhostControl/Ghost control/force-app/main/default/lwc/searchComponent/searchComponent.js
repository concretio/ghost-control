import { LightningElement, track, api } from 'lwc';

export default class SearchComponent extends LightningElement {
    @api objectName;
    @api disable = false;
    @track searchKey;
    @api placeholder=' ';
    handleChange(event){
        /* eslint-disable no-console */
        //console.log('Search Event Started ');
        const searchKey = event.target.value;
        /* eslint-disable no-console */
        event.preventDefault();
        const searchEvent = new CustomEvent(
            'change', 
            { 
                detail : {value:searchKey}
            }
        );
        this.dispatchEvent(searchEvent);
    }
}
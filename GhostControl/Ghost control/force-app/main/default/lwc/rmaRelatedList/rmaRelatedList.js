import { LightningElement ,api,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getRmaLines from'@salesforce/apex/RmaRelatedListController.getRmaLines';
export default class RmaRelatedList extends LightningElement {
    @api recordId;
    @track rmaLines = [];
    rmaLineId='';
    errorMsg;
    showEvalModal = false;
    showNewLineModal = false;

    connectedCallback(){
        this.getLines();
    }
    getLines(){
        getRmaLines({recordId:this.recordId})
        .then(result => {
            console.log('got rmaLInes successfully');
            console.log("result "+JSON.stringify(result));
            this.rmaLines = result;

        })
        .catch(error => {
            this.errorMsg = error.message;
            window.console.log(this.error);
        });
    }

    handleReceive(event){
        this.rmaLineId = event.target.value;
        console.log("rmaLineID "+event.target.value);
        this.showEvalModal = true;

        
    }
    handleCloseModal(){
        this.showEvalModal = false;
    }

    handleNewLine(){
        this.showNewLineModal = true;
    }
    handleCloseLineModal(){
        this.showNewLineModal = false;
    }
    handleNewLineSave(){
        const event = new ShowToastEvent({
            "title": "Success!",
            "message": "RMA Line saved successfully",
            "variant":"success"
        });
        this.dispatchEvent(event);
        this.getLines();
    }
}
import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import updateRmaLine from '@salesforce/apex/CustomRmaLineInputController.updateRmaLine';

export default class CustomRmaLineInput extends LightningElement {

    @api recordId;
    
    receivedDate;
    escalation = false;
    inspectionDate;
    inspectionNotes;
    inspection = false;
    insReq;
    value;
    
    errorMsg;
    
    connectedCallback() {
        
        console.log("recordid" + this.recordId);
        
    }
    @api 
    openModal(){
        this.showModal = true;
        
    }

    get options() {
        return [
            { label: 'Goodwill Stock', value: 'Goodwill Stock' },
            { label: 'Broken', value: 'broken' },
            { label: 'Other', value: 'other' },
        ];
    }
    

    handleReceivedDate(event) {
        this.receivedDate = event.detail.value;
        console.log("received date " + this.receivedDate);
    }
    handleDisposition(event) {
        this.value = event.detail.value;
        console.log("disposition value " + this.value);
       
    }

    handleEscalation(event){
        this.escalation = event.target.checked;
        console.log("escalation value " + this.escalation);
        if (!this.escalation) {
            this.inspection = false;
        }
        else {
            this.inspection = true;
        }
    }
    
    handleInspectionDate(event) {
        this.inspectionDate = event.detail.value;
        console.log("inspection date " + this.inspectionDate);
    }

    handleInspectionNotes(event) {
        this.inspectionNotes = event.detail.value;
        console.log("inspection notes " + this.inspectionNotes);
    }
    handleClick() {

        updateRmaLine({ recordId: this.recordId,receivedDate: this.receivedDate, disposition: this.value, escalation: this.escalation, inspectionDate: this.inspectionDate, inspectionNotes: this.inspectionNotes })
            .then(result => {
                console.log('updated successfully');
                this.showToast();

            })
            .catch(error => {
                this.errorMsg = error.message;
                window.console.log(this.error);
            });

        this.handleClose();
    }
    showToast() {
        console.log("inside toast  handler");

        const toastevent =new CustomEvent('toastevnt');
        this.dispatchEvent(toastevent);

    }
    
    handleClose() {
        const closeQA = new CustomEvent('close');
        // Dispatches the event.
        this.dispatchEvent(closeQA);
        
    }
}
import { api, LightningElement,track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import insertRma from '@salesforce/apex/CustomRmaInputController.insertRma';
import saveRma from '@salesforce/apex/CustomRmaInputController.saveRma';
import getCaseNumber from '@salesforce/apex/CustomRmaInputController.getCaseNumber';

export default class CustomRmaInput extends LightningElement {
    @api recordId;
    @api caseNumber;
    rmaId;
    rmaLineId;
    value;
    product;
    quantity;
    refund;
    escalation;
    case;
    opportunity;
    distributorName = ' ';
    customerPo = ' ';
    customerRga = ' ';
    customerTrack = ' ';
    distributor = false;
    showRmaID = true;
    placeholder = '';
    keyIndex = 0;

    @track showModal = true;
    @track showSpinner = false;
    @track productData = [];
    @track itemList = [
        {
            id: 0
        }
    ];
    retrievedRecordId = false;

    renderedCallback() {
        if (!this.retrievedRecordId && this.recordId) {
            
            this.retrievedRecordId = true; // Escape case from recursion
            console.log('Found recordId: ' + this.recordId);
            this.case = this.recordId;
            console.log("caseId"+this.case);
            this.getCase();
        }
    }
    

    getCase(){
        getCaseNumber({caseId:this.recordId})
        .then(result=>{

            this.caseNumber = result;

        }).catch(error =>{
            console.log(error.message);
             
         });
    }

    
    get disable(){
        if(this.recordId!=null){
            return true;
        }else{return false;}
        
    }
    addRow() {
        ++this.keyIndex;
        var newItem = [{ id: this.keyIndex }];
        this.itemList = this.itemList.concat(newItem);
    }

    removeRow(event) {
        if (this.itemList.length >= 2) {
            this.itemList = this.itemList.filter(function (element) {
                return parseInt(element.id) !== parseInt(event.target.accessKey);
            });
        }
        var filtered = this.productData.filter(function(value, index, arr){ 
            return value.index != parseInt(event.target.accessKey);
        });
        this.productData = filtered;
    }
   
    handleCreateRma(event){
        this.showSpinner = true;
       
        let self = this;
        insertRma({caseId:this.case,opportunityId:this.opportunity,purchaseLocation:this.value,distributor:this.distributorName,customerPo:this.customerPo,customerRga:this.customerRga,customerTrack:this.customerTrack})
        .then(result=>{
            self.rmaId = result.Id;
            self.createRma(self);       
            })
            .catch(error =>{
               this.errorMsg=error.message;
                self.showToastMessage('Error!',error.message,'error',self);
            });
    }
    async createRma(self){
        try{
            const result = await saveRma({ rmaId: self.rmaId , childRecords : JSON.stringify(self.productData)});
            console.log(await result);
            if(await result == 'Success'){
                self.showSpinner = false;
                self.showToastMessage('Success!','RMA created successfully','success',self);
                self.onModalClose();;
            }
        }catch(err){
            self.showSpinner = false;
            self.showToastMessage('Error!',err.message,'error',self);
        }
    }

    get options() {
        return [
            { label: 'Ghostcontrols.com', value: 'Ghostcontrols.com' },
            { label: 'Amazon', value: 'Amazon' },
            { label: 'Distributor', value: 'Distributor' },
            { label: 'Tractor Supply Company', value: 'Tractor Supply Company' },
            { label: 'Lowes Home Improvement', value: 'Lowes Home Improvement' },
            { label: 'The Home Depot', value: 'The Home Depot' },
        ];
    }
    handleChange(event){
        this.value = event.detail.value;
        if(this.value ==='Distributor'){
            this.distributor = true;
        }
        else{
            this.distributor = false; 
        }
    }
    handleSelctedProduct(event){
        console.log("product-id"+event.detail.recordId);
        this.product = event.detail.recordId;
        let bool = false;
        this.productData.forEach( (item,index) => {
            if(item.index === event.detail.index){
                bool = true;
                item.prodId = event.detail.recordId;
            }
        });
        if(bool === false){
            this.productData.push({index:event.detail.index,prodId:event.detail.recordId});
        }
    }
    handleQuantityChange(event){
        this.quantity = event.target.value;
        let bool = false;
        this.productData.forEach( (item,index) => {
            if(item.index === event.target.name){
                bool = true;
                item.quantity = event.target.value;
            }
        });
        if(bool === false){
            this.productData.push({index:event.target.name,quantity:event.target.value});
        }
    }
    handleRefund(event){
        this.refund = event.target.checked;
        console.log("refund "+this.refund);
        let bool = false;
        this.productData.forEach( (item,index) => {
            if(item.index === event.target.name){
                bool = true;
                item.refund = event.target.checked;
            }
        });
        if(bool === false){
            this.productData.push({index:event.target.name,refund:event.target.checked});
        }
    }

    handleEscalation(event){
        this.escalation = event.target.checked;
        console.log("Escalation "+this.escalation);

        let bool = false;
        this.productData.forEach( (item,index) => {
            if(item.index === event.target.name){
                bool = true;
                item.escalation = event.target.checked;
            }
        });
        if(bool === false){
            this.productData.push({index:event.target.name,escalation:event.target.checked});
        }
    }
    
    handleSelectedCase(event){
        this.case = event.detail.recordId;
        console.log("caseid "+this.case);
    } 
    handleSelectedOpp(event){
        this.opportunity = event.detail.recordId;
    }
    handleDistributor(event){
        this.distributorName = event.detail.value;
    }
    handleCustomerPo(event){
        this.customerPo = event.detail.value;
    }
    handleCustomerRga(event){
        this.customerRga = event.detail.value;
    }
    handleCustomerTrack(event){
        this.customerTrack = event.detail.value;
    }

    
    
    showToastMessage(title,message,variant,self) {
        console.log(title + message + variant);
        const toastEvent = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
            mode: 'dismissable'
        });
        self.dispatchEvent(toastEvent);
    }
    
    onModalClose(){
        this.showModal = false;
        const closeclickedevt = new CustomEvent('closeclicked', {
            detail: { close: true },
        });
        this.dispatchEvent(closeclickedevt); 
    }
   
}
public with sharing class CustomRmaInputController {
    
    @AuraEnabled
    public static In_House_Received_Line__c insertRma(Id caseId, Id opportunityId, String purchaseLocation, String distributor,String customerPo, String customerRga,String customerTrack){
        In_House_Received_Line__c rmaObj = new In_House_Received_Line__c();
        
        rmaObj.CaseId__c = caseId;
        if(opportunityId!=null){
        rmaObj.Opportunity__c = opportunityId;
        }
        if(purchaseLocation!=null)
        rmaObj.Purchase_Location__c = purchaseLocation;
        if(distributor!=null)
        rmaObj.Distributor_Name__c = distributor;
        if(customerPo!=null)
        rmaObj.Customer_PO__c = customerPo;
        if(customerRga!=null)
        rmaObj.Customer_RGA__c  = customerRga;
        if(customerTrack!=null)
        rmaObj.Customer_RGA_Tracking__c = customerTrack;

        insert rmaObj;
        return rmaObj;
    }
    @AuraEnabled
    public static string saveRma(String rmaId,String childRecords){
        try {
            List<RMA_Line__c> rmaRecords = new List<RMA_Line__c>(); 
            List<RmaWrapper> childRecordWrapper = (List<RmaWrapper>) JSON.deserialize(childRecords, List<RmaWrapper>.class);
            for(RmaWrapper rmaRecord : childRecordWrapper){
                RMA_Line__c rmaLineObj = new RMA_Line__c();
                if(rmaRecord.prodId!=null)
                rmaLineObj.Product__c = rmaRecord.prodId;

                rmaLineObj.Qty__c = Integer.valueOf(rmaRecord.quantity);

                if(rmaRecord.refund!=null)
                rmaLineObj.Request_Refund__c = Boolean.valueOf(rmaRecord.refund);
                else {
                    rmaLineObj.Request_Refund__c = false;

                }
                if(rmaRecord.escalation!=null)
                rmaLineObj.R_D_Escalation__c = Boolean.valueOf(rmaRecord.escalation);
                else{
                    rmaLineObj.R_D_Escalation__c = false;

                }

                rmaLineObj.RMA__c = rmaId;
                rmaRecords.add(rmaLineObj);
            }
            if(rmaRecords.size() > 0 ){
                UPSERT rmaRecords;
            }
            return 'Success';
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    public class RmaWrapper{
        @AuraEnabled public String prodId{get;set;}
        @AuraEnabled public String quantity{get;set;}
        @AuraEnabled public String refund{get;set;}
        @AuraEnabled public String escalation{get;set;}
    }

}
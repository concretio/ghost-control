@isTest
public class CustomLookUpControllerTest {

    @testSetup
    public static void setup(){
        //Create Product
        List<Product2>  products = new List<Product2>{  new Product2(Name='AX30K', IsActive = true), 
                                                        new Product2(Name='ABBT2', IsActive = true),
                                                        new Product2(Name='ABBT', IsActive = true)};
        
        INSERT products;
        
        Id systemAdminId = [SELECT Id FROM Profile Where Name = 'System Administrator' LIMIT 1].Id;
        Shipment_Order_Counter__c counter = new Shipment_Order_Counter__c(SetupOwnerId = SystemAdminId, Counter__c = 1);
        INSERT counter;
        
        Shipment_Order__c order = new Shipment_Order__c(Name = 'Test Order 1', Status__c = 'Production');
        INSERT order;
        
        List<Shipment_Order_Line_Item__c> lineItems = new List<Shipment_Order_Line_Item__c>{
                                                        new Shipment_Order_Line_Item__c(Shipment_Order__c = order.Id, 
                                                                                        Product__c = products[0].Id),
                                                        new Shipment_Order_Line_Item__c(Shipment_Order__c = order.Id,
                                                                                        Product__c = products[1].Id),
                                                        new Shipment_Order_Line_Item__c(Shipment_Order__c = order.Id,
                                                                                        Product__c = products[2].Id)
                                                      };
        INSERT lineItems;
        
        
    }
    
    @isTest
    public static void fetchLookupValuesTest(){
        Test.startTest();
        
            System.debug('Counter:: ' + [SELECT Counter__c from Shipment_Order_Counter__c]);
        
            List<sObject> fetchedValues = customLookUpController.fetchLookUpValues('ABBT', 
                                                                                   'Shipment_Order_Line_Item__c');
        
            System.assertEquals(2, fetchedValues.size(), 'Line Items not found');
        
        Test.stopTest();
    }
    
}
@isTest
private class TestRmaLineTriggerHelper {
    @isTest private static void testUpdateStatusOnInsert(){
        // Test data setup
        In_House_Received_Line__c rma = new In_House_Received_Line__c();
        INSERT rma;
        RMA_Line__c line = new RMA_Line__c();
        line.RMA__c = rma.Id;
        line.Request_Refund__c = true;
        line.R_D_Escalation__c = false;
        
        //Insert another line
        RMA_Line__c line2 = new RMA_Line__c();
        line2.RMA__c = rma.Id;
        line2.Request_Refund__c = false;
        line2.R_D_Escalation__c = true;
   
        //Perform test
        Test.startTest();
        Database.SaveResult result = Database.insert(line, false);
        Database.SaveResult result2 = Database.insert(line2, false);
       
       // Verify
       System.assert(result.isSuccess());
       System.assert(result2.isSuccess());
        
       //Update lines and perform another test
       line.Refund_Complete__c = true;
       line2.R_D_Escalation__c = false;
       
       Database.SaveResult updateResult = Database.update(line, false);  
       Database.SaveResult updateResult2 = Database.update(line2, false); 
       
       //Verify
       System.assert(updateResult.isSuccess());
       System.assert(updateResult2.isSuccess());
       Test.stopTest();
    }
  

}
public with sharing class CustomLookupController {
    public CustomLookupController() {

    }
    @AuraEnabled
    public static List<SObject> findRecords(String searchKey, String objectName, String searchField){
        String key = '%' + searchKey + '%';
        
        System.debug(key);
        String QUERY = 'Select Id, '+searchField+' From '+objectName +' Where '+searchField +' LIKE :key LIMIT 10';
        List<SObject> sObjectList = Database.query(QUERY);
        System.debug(sObjectList);
        return sObjectList;
    }
    @AuraEnabled
    public static List<SObject> fetchLookUpValues(String searchKeyWord, String ObjectName){
        String key = '%' + searchKeyWord + '%';
        
        String QUERY = 'Select Id, Product__r.Name,Product__c FROM '+ ObjectName +' WHERE Product__r.Name LIKE :key LIMIT 10';
        List<SObject> sObjectList = Database.query(QUERY);
        return sObjectList;
    }
}
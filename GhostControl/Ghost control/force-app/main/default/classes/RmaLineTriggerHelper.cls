/*
 * @createdDate: 27th Aug, 2021
 * @description: Handler of the RmaLineTrigger
 */
public class RmaLineTriggerHelper {
    
    /*
     * @description: This method will update status of RMA Lines  and RMAs on insertion  
     */
    
    public static void updateStatusOnInsert(List<RMA_Line__c> rmaLines){
        Set<Id> rmaIdswithRefund = new Set<ID>();
        for(RMA_Line__c line:rmaLines){
            if(line.Request_Refund__c==true){
                
                line.Status__c ='Awaiting Refund';
                rmaIdswithRefund.add(line.RMA__c);
                
            }else if(line.R_D_Escalation__c==true){
                
                line.Status__c = 'Awaiting R&D';
            }
    	}
        List<In_House_Received_Line__c> rmasToUpdate = new List<In_House_Received_Line__c>();
        for(In_House_Received_Line__c rma:[SELECT Id,Status__c,(SELECT Id,Status__c FROM RMA_Lines__r) FROM In_House_Received_Line__c WHERE ID IN:rmaIdswithRefund]){
             //Update RMA Status 
            rma.Status__c = 'Awaiting Refund';
            rmasToUpdate.add(rma);
        }
        if(rmasToUpdate.size()>0){
            UPDATE rmasToUpdate;
        }
    }
    
    /*
     * @description: This method will update status of RMA Lines  and RMAs on Updation  
     */
    
    public static void updateStatusOnUpdate(List<RMA_Line__c> rmaLines){
        Set<Id> rmaIdswithRefund = new Set<ID>();
        Set<Id> rmaIdsWithClosedLine = new Set<ID>();
        for(RMA_Line__c line:rmaLines){
            if(line.Request_Refund__c==true){
                if(line.Refund_Complete__c==true){
                    line.Status__c='Closed';
                    rmaIdsWithClosedLine.add(line.RMA__c);
                    
                }else{
                    line.Status__c ='Awaiting Refund';
                    rmaIdswithRefund.add(line.RMA__c);
                  }
            }else if(line.R_D_Escalation__c == true){
                
                line.Status__c = 'Awaiting R&D';
            }else {
                    
                line.Status__c = 'Closed';
                rmaIdsWithClosedLine.add(line.RMA__c);
                 
            }
    	}
        List<In_House_Received_Line__c> rmasToUpdate = new List<In_House_Received_Line__c>();
        
        for(In_House_Received_Line__c rma:[SELECT Id,Status__c FROM In_House_Received_Line__c WHERE ID IN:rmaIdswithRefund]){
            
            //Update RMA Status 
            rma.Status__c = 'Awaiting Refund';
            rmasToUpdate.add(rma);
        }
        
        for(In_House_Received_Line__c rmaWithClosedLine:[SELECT Id,Status__c,(SELECT Id,Status__c FROM RMA_Lines__r) FROM In_House_Received_Line__c WHERE ID IN:rmaIdsWithClosedLine]){
           Integer flag=1;
            for(RMA_Line__c rmaLine:rmaWithClosedLine.RMA_Lines__r ){
                if(rmaLine.Status__c=='Closed'){
                    flag++;
                }
            }
            if(flag == rmaWithClosedLine.RMA_Lines__r.size()){
                rmaWithClosedLine.Status__c = 'Closed';
            	rmasToUpdate.add(rmaWithClosedLine);
            }
        }
        if(rmasToUpdate.size()>0){
            UPDATE rmasToUpdate;
        }
    }

}
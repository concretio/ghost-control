public with sharing class RmaRelatedListController {
    @AuraEnabled
    public static List<RMA_Line__c> getRmaLines(Id recordId){
        
        List<RMA_Line__c> rmaLines = [SELECT Id FROM RMA_Line__c WHERE RMA__c =:recordId];
        return rmaLines;
    }
}
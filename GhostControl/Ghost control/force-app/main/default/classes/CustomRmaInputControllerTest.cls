@isTest
private class CustomRmaInputControllerTest {
    @isTest static void testInsertRma(){
        
        //Insert a case
        Case testCase = new Case();
        testCase.Reason = 'Other';
        testCase.Status = 'New';
        INSERT testCase;
        
        //Perform test
        In_House_Received_Line__c testRma = customRmaInputController.insertRma(testCase.Id,null,'Amazon',null,'test','test','test');
        System.assertEquals(testCase.Id, testRma.CaseId__c);
        testSaveRma(testRma.Id);
        
    }
    
    private static void testSaveRma(Id rmaId){
        List<customRmaInputController.RmaWrapper> wrapperList = new List<customRmaInputController.RmaWrapper>();
        //Make a PriceBook
        Pricebook2 priceBook = new Pricebook2(Name ='Service');
        INSERT priceBook;
        //Make a Product
        Product2 product = new Product2(Name ='test product');
        INSERT product;
        
        //Make another Product
        Product2 product2 = new Product2(Name ='test product2');
        INSERT product2;
        
        customRmaInputController.RmaWrapper wrapper = new customRmaInputController.RmaWrapper();
       
        wrapper.prodId = product.Id;
        wrapper.quantity = String.valueOf(13);
        wrapper.refund = Json.serialize(true);
        wrapper.escalation = Json.serialize(false);
        
        wrapperList.add(wrapper);
        //serialize rma line
        String lines = Json.serialize(wrapperList);
        
        //Perform test
        String result = customRmaInputController.saveRma(rmaId,lines);
        System.assertEquals('Success', result);
    }

}
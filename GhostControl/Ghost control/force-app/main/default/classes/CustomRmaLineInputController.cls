public with sharing class CustomRmaLineInputController {
    @AuraEnabled
    public static void updateRmaLine( Id recordId,Date receivedDate,String disposition,Boolean escalation,Date inspectionDate,String inspectionNotes){
        RMA_Line__c rmaLineObj = [SELECT Id,Received_Date__c,Product_Disposition__c,Inspection_Date__c,Inspection_Notes__c FROM RMA_Line__c WHERE ID =:recordId LIMIT 1];
        
        rmaLineObj.R_D_Escalation__c = escalation;
        if(receivedDate!=null){
        rmaLineObj.Received_Date__c = receivedDate;
        rmaLineObj.Received__c = true;
        }
        if(disposition!=null){
        rmaLineObj.Product_Disposition__c = disposition;
        }
        
        if(inspectionDate!=null){
        rmaLineObj.Inspection_Date__c = inspectionDate;
        }
        if(inspectionNotes!=null){
        rmaLineObj.Inspection_Notes__c = inspectionNotes;
        }

        update rmaLineObj;
    }

   
}
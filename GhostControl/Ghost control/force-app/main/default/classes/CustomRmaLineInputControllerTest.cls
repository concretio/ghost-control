@isTest
private class CustomRmaLineInputControllerTest {
    @isTest static void TestupdateRmaLine(){
        //Set up test data
        In_House_Received_Line__c rma = new In_House_Received_Line__c();
        INSERT rma;
      
        RMA_Line__c line = new RMA_Line__c();
        line.RMA__c = rma.Id;
        line.Qty__c = 11;
        INSERT line;
        // Perform test for null inputs
        
        customRmaLineInputController.updateRmaLine(line.Id,null,null,false,null,null);
        System.assertNotEquals(true, line.Received__c);
        
        //Perform test for valid inputs
        customRmaLineInputController.updateRmaLine(line.Id,system.today(),'Goodwill Stock',true,system.today(),'test notes');
        RMA_Line__c lineReturn = [SELECT Received__c FROM RMA_Line__c WHERE Id = :line.Id];
        System.assertEquals(true, lineReturn.Received__c);
        
        
    }
    
  

}
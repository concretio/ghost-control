/*
* @description: This trigger will update status of RMA Lines  and RMAs on Insertion/Updation  
*/
trigger RmaLineTrigger on RMA_Line__c (before insert,before update) {
    
    
    if(Trigger.isInsert){
    	RmaLineTriggerHelper.updateStatusOnInsert(Trigger.New);
    }else{
        RmaLineTriggerHelper.updateStatusOnUpdate(Trigger.New);
    }

}